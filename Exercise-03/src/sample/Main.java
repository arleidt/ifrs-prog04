package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button btn01 = new Button("Button 01");

        /* TODO 1 - Implemente uma ação */
        btn01.setOnAction(new EventHandler<ActionEvent>() {
        });

        Button btn02 = new Button("Button 02");
        /* TODO 2 - Implemente um EventHandler utilizando o recurso lambda */

        /* TODO 5 - Troque o 'StackPane' pelo 'VBox' */
        /* TODO 6 - Chame os métodos 'container.setAlignment' e 'container.setSpacing' */
        StackPane container = new StackPane();

        container.getChildren().add(btn01);
        /* TODO 3 - Adicione o 'btn02' ao container */
        /* TODO 4 - Rode o código (Shift + F10) */

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(container, 640, 480));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
